import * as R from '/rlab/mod.ts'

function sigmoid(x) {
    return 1/(R.exp(-x)+1)
}

function dSigmoid(x) {
  return R.exp(-x)/R.pow(R.exp(-x)+1, 2)
}

function softmax(x) {
  let e=R.exp(x-x.max())
  let axis=0
  return e/R.sum(e,axis)
}

function dSoftmax(x) {
  let e=R.exp(R.sub(x, R.max(x)))
  let axis = 0
  return e/R.sum(e,axis)*(1-e/R.sum(e,axis))
}