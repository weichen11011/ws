# example

## example1

```
$ deno run ex1.js
net.forward()= Node { v: 2, g: 0 }
net.backwward()
x= Node { v: 1, g: 2 } y= Node { v: 2, g: 1 } o= Node { v: 2, g: 1 }
gfx = x.g/o.g =  2 gfy = y.g/o.g= 1
```

## example2

```
$ deno run ex2.js

net.forward()= Node { v: 10, g: 0 }
net.backward()
x= Node { v: 1, g: 2 } y= Node { v: 3, g: 6 } o= Node { v: 10, g: 1 }
gfx = x.g/o.g =  2 gfy = y.g/o.g= 6
x2= Node { v: 1, g: 1 } y2= Node { v: 9, g: 1 }
```
